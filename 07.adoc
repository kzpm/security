:toc: left
:icons: font
:toclevels: 3
:toc-title: Inhoud
:sec: https://kbase.nl/security/

== {sec}[Security IT MBO]

== 7. Wetgeving en digitale beveiliging


.Doel van deze module:
|===
|Je maakt kennis met de wetgeving op het gebied van privacy 
|===


=== 7.1 Privacy en persoonsgegevens

Het hebben van een persoonlijk levenssfeer is een groot goed. Ook online.
Als security-specialist zul je vaak momenten hebben waar de wet op privacy in het geding is.
Dit begint al bij printopdrachten, waarbij soms papieren met zeer persoonlijke informatie op de printer blijven liggen. Of een medewerker, die bij het verlaten van zijn werkruimte, zijn mailbox nog open heeft staan.

Ook zeer ingrijpende zaken kunnen gebeuren:
Een medewerker van het Antoni van Leeuwenhoek ziekenhuis (Nederlands Kanker Instituut) had een harde schijf laten liggen met daarop het opschrift: "781 patientengegevens". Op de schijf bleek info te staan over de behandeling, voortgang en de levensprognose!

Uit het security beleid bleek dat de gegevens nooit op de schijf hadden mogen komen:
_Het is medewerkers van het Antoni van Leeuwenhoek ziekenhuis explicitiet verboden om vertrouwelijke gegevens zoals patientinformatie op onbeveiligde laptops, computers, harde schijven en usb sticks te zetten._

Privacy is zoals eerder gezegd, diep verankerd in de Nederlandse samenleving. Er is al heel veel omtrent privacy vastgelegd in wetten. Een van de belangrijkste is de WBP -> Wet bescherming persoonsgegevens.
Deze wet geeft aan hoe er met persoonsgegevens omgegaan dient te worden.. Strafbepalingen staan weer in de Wet op de Computercriminaliteit. Ook is het briefgeheim vastgelegd in de Grondwet. 

De WBP heeft strikte regels over persoonsinformatie en de herleidbaarheid van gegevens. Zo is bijvoorbeeld een IP-adres of een mailadres een persoonsgegeven volgens de WBP. Ze zijn beide herleidbaar naar een persoon.

Voor het bewaren van persoonsgegevens bestaan regels. Ze hangen af van het type gegevens. De gegevensbeveiliging van de leden van een snookerclub worden minder beveiligd dan informatie over iemands strafblad.

=== 7.2 Briefgeheim en privacy
De Grondwet garandeert dat telefoongesprekken, papieren post en telegraafberichten geheim zijn. Het briefgeheim is niet van toepassing op vormen van electronische communicatie, zoals mail, chat, enz.

image::https://fpf.org/wp-content/uploads/2016/05/shutterstock_335036702_R.jpg[Privacy,300x200]

=== 7.3 Opdracht Privacy stellingen

----
Je bent beheerder van een forum. Hierdoor mag je geplaatste berichten naar eigen goeddunken verwijderen en laten staan. 

- Waar. Als eigenaar ben jij verantwoordelijk voor de inhoud, dus moet je kunnen ingrijpen wanneer inhoud niet passend is.
- Niet waar. Alleen berichten, die anderen schaden, mag je verwijderen.
----

----
Ook wanneer een mail alleen maar voor mij bestemd is, mag ik de inhoud wel doorgeven aan derden, zolang ik er maar een eigen verhaal van maak.

- Waar. Als niet te herleiden is van wie de mail afkomstig is en de strekking van de mail in jouw woorden is 'verpakt', mag je het openbaar maken. Het is immers nu jouw tekst geworden. Er zit auteursrecht op email.
- Niet waar. Je mag de teksten naar eigen inzicht gebruiken. Er zit niet voor niks een "Forward" knop in je mailclient.
----

----
Als bedrijfseigenaar verbied je medewerkers prive mail te lezen tijdens werktijd. Het draagt niet bij aan de doelstelling van het bedrijf, dus mag het niet.
- Waar. Op het werk kun je privegebruik van bedrijfsmiddelen verbieden. Het draagt inderdaad niet bij aan de winst doelstellingen.
- Niet waar. Je hebt recht op privacy op je werkplek en je mag daar dus ook privezaken afhandelen, zolang het niet hinderlijk is voor anderen.
----

----
Als security-specialist heb je vanuit bedrijfsbelang het recht om werknemers van het bedrijf te monitoren en hun internetgedrag te loggen.
- Waar. Beveiliging en het bedrijfsbelang gaat boven privacy
- Niet waar. Je mag niet zomaar gaan loggen en monitoren. Privacy gaat voor bedrijfsbelang.
----


=== 7.4 Hoe bewaak je privacy op internet?

1. *Besef goed wat je doet*. Alles wat je via blogs of over fora over jezelf publiceert, staat voor eeuwig op internet. De privacywet biedt je theoretisch bescherming, maar in de praktijk is dat vaak lastiger.
2. *Wees terughoudend* met het invullen van naam- en adresgegevens op websites. In veel gevallen is je telefoonnummer nergens voor nodig.
3. *Lees de privacyverklaring* van een site voor je je registreert. Vraag de site wat er met je gegevens gebeurt, wanneer de verklaring niet duidelijk is.
4. *Gebruik een tijdelijk email adres* om je te registreren. Gebruik je echte mailadres alleen voor privecorrespondentie.
5. *Let op welke cookies een site meestuurt*. Tegenwoordig kun je vaak cookies uitzetten in een cookie statement. Ook kun je je browser zo instellen dat het geen cookies toestaat.
6. *Facebook, Homepages, LinkedIn* Alles op social media is in principe voor de hele wereld toegankelijk. Ook wanneer je de instellingen beperkt via die diensten. Immers je weet nooit wat vrienden met informatie over jou zullen gaan doen.
7. *Publiceren*. Vraag toestemming aan betrokkenen, wanneer je foto's of informatie wilt plaatsen.

 





 

